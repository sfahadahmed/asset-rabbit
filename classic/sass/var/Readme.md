### ./sass/var

This folder contains Sass files defining Sass variables corresponding to classes
included in the application's JavaScript code build when using the classic toolkit.
By default, files in this folder are mapped to the application's root namespace,
'AssetRabbit' in the same way as `"AssetRabbit/sass/src"`.