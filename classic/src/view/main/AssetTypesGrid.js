/**
 * This view is a list of asset types.
 */
Ext.define('AssetRabbit.view.main.AssetTypesGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'assettypesgrid',

    requires: [
        'AssetRabbit.store.AssetTypes'
    ],

    controller: 'assettype',

    title: 'Asset Types',

    store: {
        type: 'assettypes'
    },

    columns: [{
        xtype: 'gridcolumn',
        dataIndex: 'id',
        text: 'ID',
        width: 80
    }, {
        xtype: 'gridcolumn',
        dataIndex: 'name',
        text: 'Name',
        width: 150
    }, {
        xtype: 'gridcolumn',
        dataIndex: 'description',
        text: 'Description',
        width: 350
    }, {
        xtype: 'gridcolumn',
        dataIndex: 'serialNumberFormat',
        text: 'Serial Number Format',
        width: 100
    }, {
        xtype: 'gridcolumn',
        dataIndex: 'manufacturer',
        text: 'Manufacturer',
        width: 120
    }, {
        xtype: 'gridcolumn',
        dataIndex: 'manufacturerPartNumber',
        text: 'Manufacturer Part Number',
        width: 150
    }, {
        xtype: 'booleancolumn',
        dataIndex: 'capitalAsset',
        text: 'Capital Asset',
        width: 150,
        trueText: 'Yes',
        falseText: 'No'
    }, {
        xtype: 'gridcolumn',
        dataIndex: 'cost',
        text: 'Cost',
        width: 100
    }, {
        xtype: 'datecolumn',
        dataIndex: 'creationDate',
        text: 'Creation Date',
        width: 150
    }, {
        xtype: 'gridcolumn',
        dataIndex: 'creationUser',
        text: 'Creation User',
        width: 150
    }],

    viewConfig: {
        preserveScrollOnRefresh: true,
        preserveScrollOnReload: true
    },

    selModel: {
        selType: 'checkboxmodel',
        checkOnly: true,
        showHeaderCheckbox: true,
        listeners: {
            selectionchange: function(selModel, selections, eOpts )
            {
                 selModel.view.grid.down('#remove').setDisabled(selections.length === 0);
            }
        }
    },

    tbar: [{
        text: 'Add',
        iconCls: 'x-fa fa-plus-circle',
        itemId: 'add',
        handler: 'onAdd'
    }, {
        text: 'Remove',
        iconCls: 'x-fa fa-minus-circle',
        itemId: 'remove',
        disabled: true,
        handler: 'onRemove'
    }],

    listeners: {
        //cellclick: 'onRowSelected'
        celldblclick: 'onRowSelected',
        activate: function(grid, e)
        {
            // restore stored form window if it exist
            if(openWindows['AssetType'])
                openWindows['AssetType'].show();
        }
    }
});