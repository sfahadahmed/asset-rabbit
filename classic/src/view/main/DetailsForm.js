Ext.define('AssetRabbit.view.main.DetailsForm', {
    extend: 'Ext.window.Window',
    alias: 'widget.detailsform',

    requires: [],

    initComponent: function()
    {
        Ext.apply(this, {
        });
        this.callParent();
    },

    /*
     *
     */
    slideInFadeIn: function(config)
    {
        this.animate({
            duration: (isNaN(config.duration) == false)? config.duration : 1000,
            from: {
                opacity: 0.0
            },

            to: {
                x: config.to.x,
                opacity: 1.0
            },

            listeners: {
                afteranimate: function()
                {
                    if(config.afterAnimateHandler && typeof(config.afterAnimateHandler) == 'function')
                        config.afterAnimateHandler();
                }
            }
        });
    },

    /*
     *
     */
    slideOutFadeOut: function(config)
    {
        this.animate({
            duration: (isNaN(config.duration) == false)? config.duration : 1000,
            to: {
                x: config.to.x,
                opacity: 0.0
            },

            listeners: {
                afteranimate: function()
                {
                    if(config.afterAnimateHandler && typeof(config.afterAnimateHandler) == 'function')
                        config.afterAnimateHandler();
                }
            }
        });
    }
});