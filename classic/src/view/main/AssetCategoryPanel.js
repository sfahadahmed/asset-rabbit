Ext.define('AssetRabbit.view.main.AssetCategoryPanel', {
    extend: 'AssetRabbit.view.main.DetailPanel',
    xtype: 'assetcategorypanel',

    requires: ['AssetRabbit.view.main.DetailPanel'],

    controller: 'assetcategory',

    initComponent: function(){
        Ext.apply(this, {
            frame: true,
            defaultType: 'textfield',
            bodyPadding: 5,

            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },

            items: [{
                xtype: 'hidden',
                name: 'id',
                allowBlank: false
            }, {
                fieldLabel: 'Name',
                name: 'name',
                allowBlank: false,
                msgTarget: 'under',
                stateful: true,
                stateId: 'AssetCategoryPanel-name'
            }, {
                fieldLabel: 'Description',
                name: 'description',
                allowBlank: false,
                msgTarget: 'under',
                stateful: true,
                stateId: 'AssetCategoryPanel-description'
            }],

            dockedItems: [{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: ['->', {
                    itemId: 'save',
                    text: 'Save',
                    iconCls: 'x-fa fa-floppy-o',
                    scope: this,
                    hidden: (this.isEditForm == true)? false : true,
                    handler: this.onSave
                }, {
                    itemId: 'create',
                    text: 'Create',
                    iconCls: 'x-fa fa-star',
                    scope: this,
                    hidden: (this.isEditForm == true)? true : false,
                    handler: this.onCreate
                }]
            }]
        });

        this.callParent();
    },

    onSave: function()
    {
        var that = this;
        var form = this.getForm();

        // disable submit button
        var submitButton = this.down('#save');
        if(submitButton)
            submitButton.setDisabled(true);

        if (!this.activeRecord)
            return;

        if (form && form.isValid())
        {
            var params = form.getValues();
            var attributes = {};
            var relationships = {};

            //
            // attributes
            //
            if(params['name'] != this.activeRecord.get('name'))
                attributes['name'] = params['name'];

            if(params['description'] != this.activeRecord.get('description'))
                attributes['description'] = params['description'];

            // update the record with new changes
            form.updateRecord(this.activeRecord);

            var obj = {
                "data": {
                    "type": "assetcategories",
                    "id": params.id,
                    "attributes": attributes,
                    "relationships": relationships
                }
            };

            var win = this.findParentByType('window');

            rest = Ext.create('AssetRabbit.common.REST');
            rest.request({
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/vnd.api+json;charset=UTF-8',
                    'Accept': 'application/vnd.api+json;charset=UTF-8'
                },
                url: '/jsonapi/assetcategories/'+params.id,
                params: obj,
                callbacks: {
                    success: function(result)
                    {
                        if(submitButton)
                            submitButton.setDisabled(false);

                        if(win)
                            win.close();
                        else
                            that.onReset();

                        // reload the data store via the configured proxy
                        if(that.grid)
                        {
                            var store = that.grid.getStore();
                            if(store)
                                store.load();
                        }

                        Ext.toast({
                            title: 'Success',
                            html: 'Asset category has been updated successfully.',
                            align: 't',
                            listeners: {
                                destroy: function()
                                {
                                }
                            }
                        });
                    },

                    failure: function(result)
                    {
                        if(submitButton)
                            submitButton.setDisabled(false);

                        if(result && result.ResponseText)
                        {
                            var response = Ext.decode(result.ResponseText);
                            Ext.toast({
                                title: 'Error',
                                html: 'Cannot update a new asset category. '+(result.message)? result.message : '',
                                align: 't'
                            });
                        }
                        else
                        {
                            Ext.toast({
                                title: 'Error',
                                html: 'Cannot update a new user.',
                                align: 't'
                            });
                        }
                    }
                }
            });
        }
    },

    onCreate: function()
    {
        var that = this;
        var form = this.getForm();

        // disable submit button
        var submitButton = this.down('#create');
        if(submitButton)
            submitButton.setDisabled(true);

        if(this.grid && this.grid.getStore && form.isValid())
        {
            var store = this.grid.getStore();
            var params = form.getValues();
            var attributes = {};
            var relationships = {};

            //
            // attributes
            //
            attributes['name'] = params['name'];
            attributes['description'] = params['description'];

            var obj = {
                "data": {
                    "type": "assetcategories",
                    "id": params.id,
                    "attributes": attributes,
                    "relationships": relationships
                }
            };

            var win = this.findParentByType('window');

            rest = Ext.create('AssetRabbit.common.REST');
            rest.request({
                method: 'POST',
                headers: {
                    'Content-Type': 'application/vnd.api+json;charset=UTF-8',
                    'Accept': 'application/vnd.api+json;charset=UTF-8'
                },
                url: '/jsonapi/assetcategories/',
                params: obj,
                callbacks: {
                    success: function(result)
                    {
                        if(submitButton)
                            submitButton.setDisabled(false);

                        if(win)
                            win.close();
                        else
                            that.onReset();

                        // reload the data store via the configured proxy
                        if(that.grid)
                        {
                            var store = that.grid.getStore();
                            if(store)
                                store.load();
                        }

                        Ext.toast({
                            title: 'Success',
                            html: 'New asset category has been created successfully.',
                            align: 't',
                            listeners: {
                                destroy: function()
                                {
                                }
                            }
                        });
                    },

                    failure: function(result)
                    {
                        if(submitButton)
                            submitButton.setDisabled(false);

                        if(result && result.ResponseText)
                        {
                            var response = Ext.decode(result.ResponseText);
                            Ext.toast({
                                title: 'Error',
                                html: 'Cannot create a new asset category. '+(result.message)? result.message : '',
                                align: 't'
                            });
                        }
                        else
                        {
                            Ext.toast({
                                title: 'Error',
                                html: 'Cannot create a new asset category.',
                                align: 't'
                            });
                        }
                    }
                }
            });
        }
    }
});
