Ext.define('AssetRabbit.view.main.AssetTypePanel', {
    extend: 'AssetRabbit.view.main.DetailPanel',
    xtype: 'assettypepanel',

    requires: ['AssetRabbit.view.main.DetailPanel'],

    initComponent: function(){
        Ext.apply(this, {
            frame: true,
            defaultType: 'textfield',
            bodyPadding: 5,

            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },

            items: [{
                xtype: 'hidden',
                name: 'id',
                allowBlank: false
            }, {
                fieldLabel: 'Name',
                name: 'name',
                allowBlank: false,
                msgTarget: 'under',
                stateful: true,
                stateId: 'AssetTypePanel-name'
            }, {
                fieldLabel: 'Description',
                name: 'description',
                allowBlank: false,
                msgTarget: 'under',
                stateful: true,
                stateId: 'AssetTypePanel-description'
            }, {
                xtype: 'checkcombo',
                fieldLabel: 'Category',
                name: 'assetCategory',
                store: Ext.create('AssetRabbit.store.AssetCategories'),
                queryMode: 'local',
                valueField: 'id',
                displayField: 'name',
                multiSelect: true,
                msgTarget: 'under',
                stateful: true,
                stateId: 'AssetTypePanel-assetCategory'
            }, {
                xtype: 'textarea',
                fieldLabel: 'Serial Number Format',
                name: 'serialNumberFormat',
                allowBlank: false,
                msgTarget: 'under',
                stateful: true,
                stateId: 'AssetTypePanel-serialNumberFormat'
            }, {
                xtype: 'textarea',
                fieldLabel: 'Manufacturer',
                name: 'manufacturer',
                allowBlank: false,
                msgTarget: 'under',
                stateful: true,
                stateId: 'AssetTypePanel-manufacturer'
            }, {
                xtype: 'textarea',
                fieldLabel: 'Manufacturer Part Number',
                name: 'manufacturerPartNumber',
                allowBlank: false,
                msgTarget: 'under',
                stateful: true,
                stateId: 'AssetTypePanel-manufacturerPartNumber'
            }, {
                xtype: 'checkbox',
                fieldLabel: 'Capital Asset',
                name: 'capitalAsset',
                allowBlank: false,
                uncheckedValue: 'false',
                inputValue: 'true',
                msgTarget: 'under',
                stateful: true,
                stateId: 'AssetTypePanel-capitalAsset'
            }, {
                xtype: 'numberfield',
                fieldLabel: 'Cost',
                name: 'cost',
                allowBlank: false,
                msgTarget: 'under',
                stateful: true,
                stateId: 'AssetTypePanel-cost'
            }],

            dockedItems: [{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: ['->', {
                    itemId: 'save',
                    text: 'Save',
                    iconCls: 'x-fa fa-floppy-o',
                    scope: this,
                    hidden: (this.isEditForm == true)? false : true,
                    handler: this.onSave
                }, {
                    itemId: 'create',
                    text: 'Create',
                    iconCls: 'x-fa fa-star',
                    scope: this,
                    hidden: (this.isEditForm == true)? true : false,
                    handler: this.onCreate
                }]
            }]
        });

        this.callParent();
    },

    onSave: function()
    {
        var that = this;
        var form = this.getForm();

        if (!this.activeRecord)
            return;

        if (form && form.isValid())
        {
            var params = form.getValues();
            var attributes = {};
            var relationships = {};

            //
            // attributes
            //
            if(params['name'] != this.activeRecord.get('name'))
                attributes['name'] = params['name'];

            if(params['description'] != this.activeRecord.get('description'))
                attributes['description'] = params['description'];

            if(params['serialNumberFormat'] != this.activeRecord.get('serialNumberFormat'))
                attributes['serialNumberFormat'] = params['serialNumberFormat'];

            if(params['manufacturer'] != this.activeRecord.get('manufacturer'))
                attributes['manufacturer'] = params['manufacturer'];

            if(params['manufacturerPartNumber'] != this.activeRecord.get('manufacturerPartNumber'))
                attributes['manufacturerPartNumber'] = params['manufacturerPartNumber'];

            if(params['capitalAsset'] != this.activeRecord.get('capitalAsset'))
                attributes['capitalAsset'] = params['capitalAsset'];

            if(params['cost'] != this.activeRecord.get('cost'))
                attributes['cost'] = params['cost'];

            //
            // relationships
            //

            // asset category association
            if(params['assetCategory'] != this.activeRecord.get('assetCategory'))
            {
                var data = [];
                var categories = params['assetCategory'].toString().split(',');
                for(var i = 0; i < categories.length; ++i)
                {
                    data.push({
                        "type": "assetcategories",
                        "id": categories[i]
                    });
                }

                relationships['assetCategories'] = {
                    "data": data
                };
            }

            // update the record with new changes
            form.updateRecord(this.activeRecord);

            var obj = {
                "data": {
                    "type": "assettypes",
                    "id": params.id,
                    "attributes": attributes,
                    "relationships": relationships
                }
            };

            var win = this.findParentByType('window');

            // disable submit button
            var submitButton = this.down('#save');
            if(submitButton)
                submitButton.setDisabled(true);

            rest = Ext.create('AssetRabbit.common.REST');
            rest.request({
                method: 'PATCH',
                url: '/jsonapi/assettypes/'+params.id,
                headers: {
                    'Content-Type': 'application/vnd.api+json;charset=UTF-8',
                    'Accept': 'application/vnd.api+json;charset=UTF-8'
                },
                params: obj,
                callbacks: {
                    success: function(result)
                    {
                        if(submitButton)
                            submitButton.setDisabled(false);

                        if(win)
                            win.close();
                        else
                            that.onReset();

                        // reload the data store via the configured proxy
                        if(that.grid)
                        {
                            var store = that.grid.getStore();
                            if(store)
                                store.load();
                        }

                        Ext.toast({
                            title: 'Success',
                            html: 'Asset type has been updated successfully.',
                            align: 't',
                            listeners: {
                                destroy: function()
                                {
                                }
                            }
                        });
                    },

                    failure: function(result)
                    {
                        if(submitButton)
                            submitButton.setDisabled(false);

                        if(result && result.ResponseText)
                        {
                            var response = Ext.decode(result.ResponseText);
                            Ext.toast({
                                title: 'Error',
                                html: 'Cannot update a new asset type. '+(result.message)? result.message : '',
                                align: 't'
                            });
                        }
                        else
                        {
                            Ext.toast({
                                title: 'Error',
                                html: 'Cannot update a new asset type.',
                                align: 't'
                            });
                        }
                    }
                }
            });
        }
    },

    onCreate: function()
    {
        var that = this;
        var form = this.getForm();

        if(this.grid && this.grid.getStore && form.isValid())
        {
            var store = this.grid.getStore();
            var params = form.getValues();
            var attributes = {};
            var relationships = {};

            //
            // attributes
            //
            attributes['name'] = params['name'];
            attributes['description'] = params['description'];
            attributes['serialNumberFormat'] = params['serialNumberFormat'];
            attributes['manufacturer'] = params['manufacturer'];
            attributes['manufacturerPartNumber'] = params['manufacturerPartNumber'];
            attributes['capitalAsset'] = params['capitalAsset'];
            attributes['cost'] = params['cost'];
            attributes['creationDate'] = Ext.Date.format(new Date(), 'Y-m-d');    // creation timestamp

            //
            // relationships
            //

            // asset category association
            if(params['assetCategory'])// != '')
            {
                var data = [];
                var categories = params['assetCategory'].toString().split(',');
                for(var i = 0; i < categories.length; ++i)
                {
                    data.push({
                        "type": "assetcategories",
                        "id": categories[i]
                    });
                }

                relationships['assetCategories'] = {
                    "data": data
                };
            }

            var obj = {
                "data": {
                    "type": "assettypes",
                    "id": params.id,
                    "attributes": attributes,
                    "relationships": relationships
                }
            };

            var win = this.findParentByType('window');

            // disable submit button
            var submitButton = this.down('#create');
            if(submitButton)
                submitButton.setDisabled(true);

            rest = Ext.create('AssetRabbit.common.REST');
            rest.request({
                method: 'POST',
                headers: {
                    'Content-Type': 'application/vnd.api+json;charset=UTF-8',
                    'Accept': 'application/vnd.api+json;charset=UTF-8'
                },
                url: '/jsonapi/assettypes',
                params: obj,
                callbacks: {
                    success: function(result)
                    {
                        if(submitButton)
                            submitButton.setDisabled(false);

                        if(win)
                            win.close();
                        else
                            that.onReset();

                        // reload the data store via the configured proxy
                        if(that.grid)
                        {
                            var store = that.grid.getStore();
                            if(store)
                                store.load();
                        }

                        Ext.toast({
                            title: 'Success',
                            html: 'New asset type has been created successfully.',
                            align: 't',
                            listeners: {
                                destroy: function()
                                {
                                }
                            }
                        });
                    },

                    failure: function(result)
                    {
                        if(submitButton)
                            submitButton.setDisabled(false);

                        if(result && result.ResponseText)
                        {
                            var response = Ext.decode(result.ResponseText);
                            Ext.toast({
                                title: 'Error',
                                html: 'Cannot create a new asset type. '+(result.message)? result.message : '',
                                align: 't'
                            });
                        }
                        else
                        {
                            Ext.toast({
                                title: 'Error',
                                html: 'Cannot create a new asset type.',
                                align: 't'
                            });
                        }
                    }
                }
            });
        }
    }
});
