Ext.define('AssetRabbit.view.main.UserPanel', {
    extend: 'AssetRabbit.view.main.DetailPanel',
    xtype: 'userpanel',

    requires: ['AssetRabbit.view.main.DetailPanel'],

    controller: 'user',

    initComponent: function(){
        Ext.apply(this, {
            frame: true,
            defaultType: 'textfield',
            bodyPadding: 5,

            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },

            items: [{
                xtype: 'hidden',
                name: 'id',
                allowBlank: false
            }, {
                fieldLabel: 'Username',
                name: 'username',
                allowBlank: false,
                readOnly: (this.isEditForm == true)? true : false,
                msgTarget: 'under',
                stateful: true,
                stateId: 'UserPanel-username'
            }, {
                fieldLabel: 'Name',
                name: 'name',
                allowBlank: false,
                msgTarget: 'under',
                stateful: true,
                stateId: 'UserPanel-name'
            }, {
                fieldLabel: 'Email',
                name: 'mail',
                vtype: 'email',
                allowBlank: false,
                msgTarget: 'under',
                stateful: true,
                stateId: 'UserPanel-mail'
            }],

            dockedItems: [{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: ['->', {
                    itemId: 'save',
                    text: 'Save',
                    iconCls: 'x-fa fa-floppy-o',
                    scope: this,
                    hidden: (this.isEditForm == true)? false : true,
                    handler: this.onSave
                }, {
                    itemId: 'create',
                    text: 'Create',
                    iconCls: 'x-fa fa-star',
                    scope: this,
                    hidden: (this.isEditForm == true)? true : false,
                    handler: this.onCreate
                }]
            }]
        });

        this.callParent();
    },

    onSave: function()
    {
        var that = this;
        var form = this.getForm();

        if (!this.activeRecord)
            return;

        if (form && form.isValid())
        {
            var params = form.getValues();
            var attributes = {};
            var relationships = {};

            //
            // attributes
            //
            if(params['username'] != this.activeRecord.get('username'))
                attributes['username'] = params['username'];

            if(params['name'] != this.activeRecord.get('name'))
                attributes['name'] = params['name'];

            if(params['mail'] != this.activeRecord.get('mail'))
                attributes['mail'] = params['mail'];

            // update the record with new changes
            form.updateRecord(this.activeRecord);

            var obj = {
                "data": {
                    "type": "users",
                    "id": params.id,
                    "attributes": attributes,
                    "relationships": relationships
                }
            };

            var win = this.findParentByType('window');

            // disable submit button
            var submitButton = this.down('#save');
            if(submitButton)
                submitButton.setDisabled(true);

            rest = Ext.create('AssetRabbit.common.REST');
            rest.request({
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/vnd.api+json;charset=UTF-8',
                    'Accept': 'application/vnd.api+json;charset=UTF-8'
                },
                url: '/jsonapi/users/'+params.id,
                params: obj,
                callbacks: {
                    success: function(result)
                    {
                        if(submitButton)
                            submitButton.setDisabled(false);

                        if(win)
                            win.close();
                        else
                            that.onReset();

                        // reload the data store via the configured proxy
                        if(that.grid)
                        {
                            var store = that.grid.getStore();
                            if(store)
                                store.load();
                        }

                        Ext.toast({
                            title: 'Success',
                            html: 'User has been updated successfully.',
                            align: 't',
                            listeners: {
                                destroy: function()
                                {
                                }
                            }
                        });
                    },

                    failure: function(result)
                    {
                        if(submitButton)
                            submitButton.setDisabled(false);

                        if(result && result.ResponseText)
                        {
                            var response = Ext.decode(result.ResponseText);
                            Ext.toast({
                                title: 'Error',
                                html: 'Cannot update a new user. '+(result.message)? result.message : '',
                                align: 't'
                            });
                        }
                        else
                        {
                            Ext.toast({
                                title: 'Error',
                                html: 'Cannot update a new user.',
                                align: 't'
                            });
                        }
                    }
                }
            });
        }
    },

    onCreate: function()
    {
        var that = this;
        var form = this.getForm();

        if(this.grid && this.grid.getStore && form.isValid())
        {
            var store = this.grid.getStore();
            var params = form.getValues();
            var attributes = {};
            var relationships = {};

            //
            // attributes
            //
            attributes['username'] = params['username'];
            attributes['name'] = params['name'];
            attributes['mail'] = params['mail'];

            var obj = {
                "data": {
                    "type": "users",
                    "id": params.id,
                    "attributes": attributes,
                    "relationships": relationships
                }
            };

            var win = this.findParentByType('window');

            // disable submit button
            var submitButton = this.down('#create');
            if(submitButton)
                submitButton.setDisabled(true);

            rest = Ext.create('AssetRabbit.common.REST');
            rest.request({
                method: 'POST',
                headers: {
                    'Content-Type': 'application/vnd.api+json;charset=UTF-8',
                    'Accept': 'application/vnd.api+json;charset=UTF-8'
                },
                url: '/jsonapi/users',
                params: obj,
                callbacks: {
                    success: function(result)
                    {
                        if(submitButton)
                            submitButton.setDisabled(false);

                        if(win)
                            win.close();
                        else
                            that.onReset();

                        // reload the data store via the configured proxy
                        if(that.grid)
                        {
                            var store = that.grid.getStore();
                            if(store)
                                store.load();
                        }

                        Ext.toast({
                            title: 'Success',
                            html: 'New user has been created successfully.',
                            align: 't',
                            listeners: {
                                destroy: function()
                                {
                                }
                            }
                        });
                    },

                    failure: function(result)
                    {
                        if(submitButton)
                            submitButton.setDisabled(false);

                        if(result && result.ResponseText)
                        {
                            var response = Ext.decode(result.ResponseText);
                            Ext.toast({
                                title: 'Error',
                                html: 'Cannot create a new user. '+(result.message)? result.message : '',
                                align: 't'
                            });
                        }
                        else
                        {
                            Ext.toast({
                                title: 'Error',
                                html: 'Cannot update a new user.',
                                align: 't'
                            });
                        }
                    }
                }
            });
        }
    }
});
