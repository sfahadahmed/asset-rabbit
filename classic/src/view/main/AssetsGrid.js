/**
 * This view is a list of assets.
 */
Ext.define('AssetRabbit.view.main.AssetsGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'assetsgrid',

    requires: [
        'AssetRabbit.store.Assets',
        'AssetRabbit.store.AssetTypes',
        'AssetRabbit.store.AssetCategories',
        'AssetRabbit.store.Users'
    ],

    controller: 'asset',

    title: 'Assets',

    store: {
        type: 'assets'
    },

    columns: [/*{
        dataIndex: 'assetCategoryId',
        text: 'Asset Category',
        width: 100
    }, */{
        xtype: 'gridcolumn',
        dataIndex: 'id',
        text: 'ID',
        width: 80
    }, {
        xtype: 'gridcolumn',
        dataIndex: 'description',
        text: 'Description',
        flex: 1
    }, {
        xtype: 'gridcolumn',
        dataIndex: 'serialNumber',
        text: 'Serial Number',
        width: 150
    }, {
        xtype: 'datecolumn',
        dataIndex: 'creationDate',
        text: 'Creation Date',
        width: 150
        //renderer: Ext.util.Format.dateRenderer('m-d-Y g:i A')
        /*renderer: function(value){
            if(value)
            {
                var d =new Date(value);
                //return Ext.Date.format(d, 'm-d-Y g:i A');
                return d.toString();
            }
            else
            {
                return '---';
            }
        }*/
    }, {
        xtype: 'datecolumn',
        dataIndex: 'deliveryDate',
        text: 'Delivery Date',
        width: 150
        //renderer: Ext.util.Format.dateRenderer('m-d-Y g:i A')
    }/*, {
        xtype: 'widgetcolumn',
        widget: {
            xtype: 'button',
            text: 'Edit',
            scale: 'small'
        }
    }*/ /*{
        dataIndex: 'capital',
        text: 'Capital',
        width: 100
    }, {
        dataIndex: 'cost',
        text: 'Cost',
        width: 100
    }, {
        dataIndex: 'partNumber',
        text: 'Part Number',
        width: 100
    },*/ /*, {
        dataIndex: 'location',
        text: 'Location',
        width: 100
    }*/ /*{
        dataIndex: 'nextServiceDate',
        text: 'Next Service Date',
        width: 150
    }*/],

    viewConfig: {
        preserveScrollOnRefresh: true,
        preserveScrollOnReload: true
    },

    selModel: {
        selType: 'checkboxmodel',
        checkOnly: true,
        showHeaderCheckbox: true,
        listeners: {
            selectionchange: function(selModel, selections, eOpts )
            {
                 selModel.view.grid.down('#remove').setDisabled(selections.length === 0);
            }
        }
    },

    tbar: [{
        text: 'Create New Asset',
        iconCls: 'x-fa fa-plus-circle',
        itemId: 'add',
        handler: 'onAdd'
    }, {
        text: 'Remove Asset',
        iconCls: 'x-fa fa-minus-circle',
        itemId: 'remove',
        disabled: true,
        handler: 'onRemove'
    }, {
        text: 'Update Asset'
    }, {
        text: 'Part Numbers'
    }, {
        text: 'Manage Kit'
    }],

    listeners: {
        //cellclick: 'onRowSelected',
        celldblclick: 'onRowSelected',
        activate: function(grid, e)
        {
            // restore stored form window if it exist
            if(openWindows['Asset'])
                openWindows['Asset'].show();
        }
    }
});