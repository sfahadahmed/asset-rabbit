/**
 * This view is a list of asset categories.
 */
Ext.define('AssetRabbit.view.main.AssetCategoriesGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'assetcategoriesgrid',

    requires: [
        'AssetRabbit.store.AssetCategories'
    ],

    controller: 'assetcategory',

    title: 'Asset Categories',

    store: {
        type: 'assetcategories'
    },

    columns: [{
        dataIndex: 'id',
        text: 'ID',
        width: 80
    }, {
        dataIndex: 'name',
        text: 'Name',
        width: 150
    }, {
        dataIndex: 'description',
        text: 'Description',
        flex: 1
    }],

    viewConfig: {
        preserveScrollOnRefresh: true,
        preserveScrollOnReload: true
    },

    selModel: {
        selType: 'checkboxmodel',
        checkOnly: true,
        showHeaderCheckbox: true,
        listeners: {
            selectionchange: function(selModel, selections, eOpts )
            {
                 selModel.view.grid.down('#remove').setDisabled(selections.length === 0);
            }
        }
    },

    tbar: [{
        text: 'Add',
        iconCls: 'x-fa fa-plus-circle',
        itemId: 'add',
        handler: 'onAdd'
    }, {
        text: 'Remove',
        iconCls: 'x-fa fa-minus-circle',
        itemId: 'remove',
        disabled: true,
        handler: 'onRemove'
    }],

    listeners: {
        //cellclick: 'onRowSelected'
        celldblclick: 'onRowSelected',
        activate: function(grid, e)
        {
            // restore stored form window if it exist
            if(openWindows['AssetCategory'])
                openWindows['AssetCategory'].show();
        }
    }
});