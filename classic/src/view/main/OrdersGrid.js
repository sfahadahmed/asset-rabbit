/**
 * This view is a list of orders.
 */
Ext.define('AssetRabbit.view.main.OrdersGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'ordersgrid',

    requires: [
        'AssetRabbit.store.Orders'
    ],

    controller: 'order',

    title: 'Orders',

    store: {
        type: 'orders'
    },

    columns: [{
        dataIndex: 'id',
        text: 'ID',
        width: 80
    }, {
        dataIndex: 'description',
        text: 'Description',
        flex: 1
    }, {
        dataIndex: 'assignedTo',
        text: 'Assigned To',
        width: 200
    }, {
        dataIndex: 'poNumber',
        text: 'PO Number',
        width: 200
    }, {
        dataIndex: 'status',
        text: 'Status',
        width: 100
    }, {
        dataIndex: 'notes',
        text: 'Notes',
        flex: 1
    }, {
        dataIndex: 'creationDate',
        text: 'Creation Date',
        width: 150
    }, {
        dataIndex: 'deliveryDate',
        text: 'Delivery Date',
        width: 150
    }],

    viewConfig: {
        preserveScrollOnRefresh: true,
        preserveScrollOnReload: true
    },

    selModel: {
        selType: 'checkboxmodel',
        checkOnly: true,
        showHeaderCheckbox: true,
        listeners: {
            selectionchange: function(selModel, selections, eOpts )
            {
                 selModel.view.grid.down('#remove').setDisabled(selections.length === 0);
            }
        }
    },

    tbar: [{
        text: 'Add',
        iconCls: 'x-fa fa-plus-circle',
        itemId: 'add',
        handler: 'onAdd'
    }, {
        text: 'Remove',
        iconCls: 'x-fa fa-minus-circle',
        itemId: 'remove',
        disabled: true,
        handler: 'onRemove'
    }],

    listeners: {
        //cellclick: 'onRowSelected'
        celldblclick: 'onRowSelected'
    }
});