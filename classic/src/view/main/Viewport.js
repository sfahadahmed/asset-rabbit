Ext.define('AssetRabbit.view.main.Viewport', {
    extend: 'Ext.container.Viewport',
    xtype: 'mainviewport',

    requires: [
        'Ext.button.Segmented',
        'Ext.list.Tree'
    ],

    controller: 'mainviewport',
    viewModel: {
        type: 'mainviewport'
    },

    //controller: 'main',
    //viewModel: 'main',

    cls: 'sencha-dash-viewport',
    itemId: 'mainView',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    listeners: {
        render: 'onMainViewRender'
    },

    items: [
        {
            xtype: 'toolbar',
            cls: 'sencha-dash-dash-headerbar toolbar-btn-shadow',
            height: 64,
            itemId: 'headerBar',
            items: [
                {
                    xtype: 'component',
                    reference: 'senchaLogo',
                    cls: 'sencha-logo',
                    //html: '<div class="main-logo"><img src="resources/images/sencha-icon.png">Sencha</div>',
                    //html: '<div class="main-logo"><img src="classic/resources/images/asset-rabbit-icon.png">Asset Rabbit</div>',
                    html: '<div class="main-logo"><img src="classic/resources/images/ar-logo-37x36.png">Asset Rabbit</div>',
                    //html: '<div class="main-logo" style="padding-top: 0px; text-align: center;"><img src="resources/images/Asset-Rabbit-Logo.png" width="170" height=50"></div>',
                    width: 250
                },
                {
                    margin: '0 0 0 8',
                    cls: 'delete-focus-bg',
                    iconCls:'x-fa fa-navicon',
                    id: 'main-navigation-btn',
                    handler: 'onToggleNavigationSize'
                },
                {
                    xtype: 'tbspacer',
                    flex: 1
                },
                {
                    cls: 'delete-focus-bg',
                    iconCls:'x-fa fa-shopping-cart',
                    href: '#cart',
                    hrefTarget: '_self',
                    tooltip: 'View Cart'
                },
                {
                    cls: 'delete-focus-bg',
                    iconCls:'x-fa fa-gear',
                    href: '#settings',
                    hrefTarget: '_self',
                    tooltip: 'Settings'
                },
                {
                    cls: 'delete-focus-bg',
                    iconCls:'x-fa fa-question',
                    href: '#help',
                    hrefTarget: '_self',
                    tooltip: 'Help'
                }, {
                    cls: 'delete-focus-bg',
                    iconCls:'x-fa fa-sign-out',
                    href: '#logout',
                    hrefTarget: '_self',
                    tooltip: 'Logout'
                }, {
                    xtype: 'tbtext',
                    text: 'Goff Smith',
                    cls: 'top-user-name'
                }, {
                    xtype: 'component',
                    cls: 'header-right-profile-image',
                    html: '<a href="#profile"><img src="classic/resources/images/user-profile/2.png" width="35" height="35" class="header-right-profile-image" alt="View Profile" title="View Profile"></a>',
                    width: 35
                }
                /*{
                    cls: 'delete-focus-bg',
                    iconCls:'x-fa fa-search',
                    href: '#search',
                    hrefTarget: '_self',
                    tooltip: 'See latest search'
                },*/ 
                /*{
                    cls: 'delete-focus-bg',
                    iconCls:'x-fa fa-envelope',
                    href: '#email',
                    hrefTarget: '_self',
                    tooltip: 'Check your email'
                },
                {
                    cls: 'delete-focus-bg',
                    iconCls:'x-fa fa-bell'
                },*/
                /*{
                    cls: 'delete-focus-bg',
                    iconCls:'x-fa fa-th-large',
                    href: '#profile',
                    hrefTarget: '_self',
                    tooltip: 'See your profile'
                },*/
                
                /*{
                    xtype: 'image',
                    cls: 'header-right-profile-image',
                    href: '#profile',
                    height: 35,
                    width: 35,
                    alt: 'View Profile',
                    src: 'classic/resources/images/user-profile/2.png'
                },*/
            ]
        },
        {
            xtype: 'maincontainerwrap',
            id: 'main-view-detail-wrap',
            reference: 'mainContainerWrap',
            flex: 1,
            items: [
                {
                    xtype: 'treelist',
                    reference: 'navigationTreeList',
                    itemId: 'navigationTreeList',
                    ui: 'navigation',
                    store: 'NavigationTree',
                    width: 250,
                    expanderFirst: false,
                    expanderOnly: false,
                    listeners: {
                        selectionchange: 'onNavigationTreeSelectionChange'
                    }
                },
                {
                    xtype: 'container',
                    flex: 1,
                    reference: 'mainCardPanel',
                    cls: 'sencha-dash-right-main-container',
                    itemId: 'contentPanel',
                    layout: {
                        type: 'card',
                        anchor: '100%'
                    }
                }
            ]
        }
    ]
});
