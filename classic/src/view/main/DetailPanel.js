Ext.define('AssetRabbit.view.main.DetailPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'detailpanel',

    requires: [
        'Ext.form.Panel',
        'Ext.form.field.Text',
        'Ext.window.Toast'
    ],

    initComponent: function(){
        var that = this;
        Ext.apply(this, {
            frame: true,
            defaultType: 'textfield',
            bodyPadding: 5,

            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },

            listeners: {
                afterrender: function(form, e)
                {
                    if(that.activeRecord)
                        that.setActiveRecord(that.activeRecord);
                }
            }
        });
        this.callParent();
    },

    setActiveRecord: function(record)
    {
        this.activeRecord = record;
        if (record)
            this.getForm().loadRecord(record);
        else
            this.getForm().reset();
    },

    onReset: function()
    {
        this.setActiveRecord(null);
        this.getForm().reset();
    }
});
