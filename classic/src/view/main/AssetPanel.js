Ext.define('AssetRabbit.view.main.AssetPanel', {
    extend: 'AssetRabbit.view.main.DetailPanel',
    xtype: 'assetpanel',

    requires: ['AssetRabbit.view.main.DetailPanel'],

    controller: 'asset',

    initComponent: function(){
        var that = this;
        var assetTypeStore = Ext.create('AssetRabbit.store.AssetTypes');
        var categoryStore = Ext.create('AssetRabbit.store.AssetCategories');
        var assetTypeField = Ext.create({
            xtype: 'combobox',
            fieldLabel: 'Asset Type',
            name: 'assetType',
            store: assetTypeStore,
            queryMode: 'local',
            valueField: 'id',
            displayField: 'name',
            msgTarget: 'under',
            stateful: true,
            stateId: 'AssetPanel-assetType'
        });

        Ext.apply(this, {
            frame: true,
            defaultType: 'textfield',
            bodyPadding: 5,
            //title: (this.isEditForm)? 'Edit' : 'Add New',

            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },

            items: [{
                xtype: 'hidden',
                name: 'id',
                allowBlank: false
            }, {
                fieldLabel: 'Description',
                name: 'description',
                allowBlank: false,
                msgTarget: 'under',
                stateful: true,
                stateId: 'AssetPanel-description'
            }, {
                fieldLabel: 'Serial Number',
                name: 'serialNumber',
                allowBlank: false,
                msgTarget: 'under',
                stateful: true,
                stateId: 'AssetPanel-serialNumber'
            }, {
                xtype: 'datefield',
                fieldLabel: 'Delivery Date',
                name: 'deliveryDate',
                allowBlank: false,
                msgTarget: 'under',
                stateful: true,
                stateId: 'AssetPanel-deliveryDate'
            }, {
                xtype: 'combobox',
                fieldLabel: 'Category',
                name: 'assetCategory',
                store: categoryStore,
                queryMode: 'local',
                valueField: 'id',
                displayField: 'name',
                msgTarget: 'under',
                stateful: true,
                stateId: 'AssetPanel-assetCategory',
                listeners: {
                    change: function(field, newValue, oldValue, eOpts)
                    {
                        if(newValue)
                        {
                            assetTypeStore.clearFilter();
                            assetTypeStore.filterBy(function(record, that){
                                var categoryIds = record.get('assetCategory');
                                if(categoryIds.indexOf(newValue) != -1)
                                    return true;    // donot filter
                                else
                                    return false;   // filter
                            });
                        }
                        else
                            assetTypeStore.clearFilter();
                    },
                    select: function(field)
                    {
                        // Clear previous field value. This helps prevent an issue when switching among different categories
                        // showing previous category's associated asset type.
                        assetTypeField.setValue('');
                    }
                }
            }, assetTypeField, {
                xtype: 'combobox',
                fieldLabel: 'Owner',
                name: 'owner',
                store: Ext.create('AssetRabbit.store.Users'),
                queryMode: 'local',
                valueField: 'id',
                displayField: 'name',
                msgTarget: 'under',
                stateful: true,
                stateId: 'AssetPanel-owner'
            }],

            dockedItems: [{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: ['->', {
                    itemId: 'save',
                    text: 'Save',
                    iconCls: 'x-fa fa-floppy-o',
                    scope: this,
                    hidden: (this.isEditForm == true)? false : true,
                    handler: this.onSave
                }, {
                    itemId: 'create',
                    text: 'Create',
                    iconCls: 'x-fa fa-star',
                    scope: this,
                    hidden: (this.isEditForm == true)? true : false,
                    handler: this.onCreate
                }]
            }]
        });
        
        this.on('beforerender', function(form, e)
        {
            var win = form.up('window');
            if(!win)
            {
                that.setTitle((that.isEditForm == true)? 'Edit Asset': 'Add New Asset');
            }
        });

        this.callParent();
    },

    onSave: function()
    {
        var that = this;
        var form = this.getForm();

        if (!this.activeRecord)
            return;

        if (form && form.isValid())
        {
            var params = form.getValues();
            var attributes = {};
            var relationships = {};

            //
            // attributes
            //
            if(params['description'] != this.activeRecord.get('description'))
                attributes['description'] = params['description'];

            if(params['serialNumber'] != this.activeRecord.get('serialNumber'))
                attributes['serialNumber'] = params['serialNumber'];

            if(params['deliveryDate'] != this.activeRecord.get('deliveryDate'))
            {
                var deliveryDate = new Date(params['deliveryDate']);
                attributes['deliveryDate'] = Ext.Date.format(deliveryDate, 'Y-m-d');//'m/d/Y'
            }

            //
            // relationships
            //
            
            // asset category association
            if(params['assetCategory'] != this.activeRecord.get('assetCategory'))
            {
                relationships['assetCategory'] = {
                    "data": {
                        "type": "assetcategories",
                        "id": params['assetCategory']
                    }
                };
            }

            // asset type association
            if(params['assetType'] != this.activeRecord.get('assetType'))
            {
                relationships['assetType'] = {
                    "data": {
                        "type": "assettypes",
                        "id": params['assetType']
                    }
                };
            }

            // owner association
            if(params['owner'] != this.activeRecord.get('owner'))
            {
                relationships['owner'] = {
                    "data": {
                        "type": "users",
                        "id": params['owner']
                    }
                };
            }

            // update the record with new changes
            form.updateRecord(this.activeRecord);

            var obj = {
                "data": {
                    "type": "assets",
                    "id": params.id,
                    "attributes": attributes,
                    "relationships": relationships
                }
            };

            var win = this.findParentByType('window');

            // disable submit button
            var submitButton = this.down('#save');
            if(submitButton)
                submitButton.setDisabled(true);

            rest = Ext.create('AssetRabbit.common.REST');
            rest.request({
                method: 'PATCH',
                url: '/jsonapi/assets/'+params.id,
                headers: {
                    'Content-Type': 'application/vnd.api+json;charset=UTF-8',
                    'Accept': 'application/vnd.api+json;charset=UTF-8'
                },
                params: obj,
                callbacks: {
                    success: function(result)
                    {
                        if(submitButton)
                            submitButton.setDisabled(false);

                        if(win)
                            win.close();
                        else
                            that.onReset();

                        // reload the data store via the configured proxy
                        if(that.grid)
                        {
                            var store = that.grid.getStore();
                            if(store)
                                store.load();
                        }

                        Ext.toast({
                            title: 'Success',
                            html: 'Asset has been updated successfully.',
                            align: 't',
                            listeners: {
                                destroy: function()
                                {
                                }
                            }
                        });
                    },

                    failure: function(result)
                    {
                        if(submitButton)
                            submitButton.setDisabled(false);

                        if(result && result.ResponseText)
                        {
                            var response = Ext.decode(result.ResponseText);
                            Ext.toast({
                                title: 'Error',
                                html: 'Cannot update a new asset. '+(result.message)? result.message : '',
                                align: 't'
                            });
                        }
                        else
                        {
                            Ext.toast({
                                title: 'Error',
                                html: 'Cannot update a new asset.',
                                align: 't'
                            });
                        }
                    }
                }
            });
        }
    },

    onCreate: function()
    {
        var that = this;
        var form = this.getForm();

        if(form.isValid())//if(this.grid && this.grid.getStore && form.isValid())
        {
            var params = form.getValues();
            var attributes = {};
            var relationships = {};

            //
            // attributes
            //
            attributes['description'] = params['description'];
            attributes['serialNumber'] = params['serialNumber'];
            attributes['creationDate'] = Ext.Date.format(new Date(), 'Y-m-d');    // creation timestamp
            if(params['deliveryDate'])
            {
                var deliveryDate = new Date(params['deliveryDate']);
                attributes['deliveryDate'] = Ext.Date.format(deliveryDate, 'Y-m-d');//'m/d/Y'
            }
            else
            {
                attributes['deliveryDate'] = Ext.Date.format(new Date(), 'Y-m-d');    // default delivery date is current
            }

            //
            // relationships
            //

            // asset category association
            if(params['assetCategory'] != '')
            {
                relationships['assetCategory'] = {
                    "data": {
                        "type": "assetcategories",
                        "id": params['assetCategory']
                    }
                };
            }

            // asset type association
            if(params['assetType'] != '')
            {
                relationships['assetType'] = {
                    "data": {
                        "type": "assettypes",
                        "id": params['assetType']
                    }
                };
            }

            // owner association
            if(params['owner'] != '')
            {
                relationships['owner'] = {
                    "data": {
                        "type": "users",
                        "id": params['owner']
                    }
                };
            }

            var obj = {
                "data": {
                    "type": "assets",
                    "id": params.id,
                    "attributes": attributes,
                    "relationships": relationships
                }
            };

            var win = this.findParentByType('window');

            // disable submit button
            var submitButton = this.down('#create');
            if(submitButton)
                submitButton.setDisabled(true);

            rest = Ext.create('AssetRabbit.common.REST');
            rest.request({
                method: 'POST',
                headers: {
                    'Content-Type': 'application/vnd.api+json;charset=UTF-8',
                    'Accept': 'application/vnd.api+json;charset=UTF-8'
                },
                url: '/jsonapi/assets',
                params: obj,
                callbacks: {
                    success: function(result)
                    {
                        if(submitButton)
                            submitButton.setDisabled(false);

                        if(win)
                            win.close();
                        else
                            that.onReset();

                        // reload the data store via the configured proxy
                        if(that.grid)
                        {
                            var store = that.grid.getStore();
                            if(store)
                                store.load();
                        }

                        Ext.toast({
                            title: 'Success',
                            html: 'New asset has been created successfully.',
                            align: 't',
                            listeners: {
                                destroy: function()
                                {
                                }
                            }
                        });
                    },

                    failure: function(result)
                    {
                        if(submitButton)
                            submitButton.setDisabled(false);

                        if(result && result.ResponseText)
                        {
                            var response = Ext.decode(result.ResponseText);
                            Ext.toast({
                                title: 'Error',
                                html: 'Cannot update a new asset. '+(result.message)? result.message : '',
                                align: 't'
                            });
                        }
                        else
                        {
                            Ext.toast({
                                title: 'Error',
                                html: 'Cannot create a new asset.',
                                align: 't'
                            });
                        }
                    }
                }
            });
        }
    }
});
