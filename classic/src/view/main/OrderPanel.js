Ext.define('AssetRabbit.view.main.OrderPanel', {
    extend: 'AssetRabbit.view.main.DetailPanel',
    xtype: 'orderpanel',

    requires: ['AssetRabbit.view.main.DetailPanel'],

    initComponent: function(){
        Ext.apply(this, {
            frame: true,
            defaultType: 'textfield',
            bodyPadding: 5,

            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },

            items: [{
                fieldLabel: 'Description',
                name: 'description',
                allowBlank: false
            }, {
                fieldLabel: 'PO Number',
                name: 'poNumber',
                allowBlank: true
            }, {
                xtype: 'combobox',
                fieldLabel: 'Assigned To',
                name: 'assignedTo',
                store: Ext.create('Ext.data.Store', {
                    fields: ['username', 'name'],
                    data : [
                        {'username': 'Jack Thompson', 'name': 'Jack Thompson'},
                        {'username': 'Tom Lewis', 'name': 'Tom Lewis'}
                    ]
                }),
                queryMode: 'local',
                displayField: 'name',
                valueField: 'username'
            }, {
                xtype: 'combobox',
                fieldLabel: 'Status',
                name: 'status',
                store: Ext.create('Ext.data.Store', {
                    fields: ['status', 'name'],
                    data : [
                        {'status': 'new', 'name': 'New'},
                        {'status': 'open', 'name': 'Open'},
                        {'status': 'closed', 'name': 'Closed'},
                        {'status': 'onhold', 'name': 'On Hold'}
                    ]
                }),
                queryMode: 'local',
                displayField: 'name',
                valueField: 'status'
                //value: (this.isEditForm == true)? this.activeRecord.get('status') : 'new'
            }, {
                xtype: 'textarea',
                fieldLabel: 'Notes',
                name: 'notes',
                allowBlank: true
            }],

            dockedItems: [{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: ['->', {
                    itemId: 'save',
                    text: 'Save',
                    iconCls: 'x-fa fa-floppy-o',
                    scope: this,
                    hidden: (this.isEditForm == true)? false : true,
                    handler: this.onSave
                }, {
                    itemId: 'create',
                    text: 'Create',
                    iconCls: 'x-fa fa-star',
                    scope: this,
                    hidden: (this.isEditForm == true)? true : false,
                    handler: this.onCreate
                }]
            }]
        });

        this.callParent();
    },

    onSave: function()
    {
        var form = this.getForm();

        if (!this.activeRecord)
            return;

        if (form && form.isValid())
        {
            form.updateRecord(this.activeRecord);

            var win = this.findParentByType('window');
            if(win)
                win.close();
            else
                this.onReset();
        }
    },

    onCreate: function()
    {
        var form = this.getForm();
        if(this.grid && this.grid.getStore && form.isValid())
        {
            var store = this.grid.getStore();
            var values = form.getValues();
            var lastRecord = store.getAt(store.getCount() - 1);

            values['id'] = (lastRecord != null)? lastRecord.get('id') + 1 : 0;
            values['creationDate'] = new Date();

            var rec = new AssetRabbit.model.Order(values);
            store.insert(0, rec);

            var win = this.findParentByType('window');
            if(win)
                win.close();
            else
                this.onReset();
        }
    }
});
