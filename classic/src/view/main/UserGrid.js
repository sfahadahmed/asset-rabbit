/**
 * This view is a list of users.
 */
Ext.define('AssetRabbit.view.main.UsersGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'usersgrid',

    requires: ['AssetRabbit.store.Users'],

    controller: 'user',

    title: 'Users',

    store: {
        type: 'users'
    },

    columns: [{
        xtype: 'gridcolumn',
        dataIndex: 'id',
        text: 'ID',
        width: 80
    }, {
        xtype: 'gridcolumn',
        dataIndex: 'username',
        text: 'Username',
        width: 150
    }, {
        xtype: 'gridcolumn',
        dataIndex: 'name',
        text: 'Name',
        flex: 1
    }],

    viewConfig: {
        preserveScrollOnRefresh: true,
        preserveScrollOnReload: true
    },

    selModel: {
        selType: 'checkboxmodel',
        checkOnly: true,
        showHeaderCheckbox: true,
        listeners: {
            selectionchange: function(selModel, selections, eOpts )
            {
                 selModel.view.grid.down('#remove').setDisabled(selections.length === 0);
            }
        }
    },

    tbar: [{
        text: 'Add',
        iconCls: 'x-fa fa-plus-circle',
        itemId: 'add',
        handler: 'onAdd'
    }, {
        text: 'Remove',
        iconCls: 'x-fa fa-minus-circle',
        itemId: 'remove',
        disabled: true,
        handler: 'onRemove'
    }],

    listeners: {
        //cellclick: 'onRowSelected'
        celldblclick: 'onRowSelected',
        activate: function(grid, e)
        {
            // restore stored form window if it exist
            if(openWindows['User'])
                openWindows['User'].show();
        }
    }
});