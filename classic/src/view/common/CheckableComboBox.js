Ext.define('AssetRabbit.view.common.CheckableComboBox', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'checkcombo',
    requires: ['Ext.form.field.ComboBox'],

    initComponent: function(){
        var that = this;

        Ext.apply(this, {
            listConfig: {
                getInnerTpl: function(displayField) {
                    return '<div class="x-combo-list-item"><img src="' + Ext.BLANK_IMAGE_URL + '" class="chkCombo-default-icon chkCombo" /> {'+ that.displayField +'} </div>';
                }
            }
        });

        this.callParent();
    }
});

// eof