Ext.define('AssetRabbit.view.authentication.Login', {
    extend: 'AssetRabbit.view.authentication.LockingWindow',
    xtype: 'login',

    requires: [
        'AssetRabbit.view.authentication.Dialog',
        'Ext.container.Container',
        'Ext.form.field.Text',
        'Ext.form.field.Checkbox',
        'Ext.button.Button'
    ],

    title: 'Asset Rabbit',
    defaultFocus: 'authdialog', // Focus the Auth Form to force field focus as well

    items: [
        {
            xtype: 'authdialog',
            defaultButton : 'loginButton',
            autoComplete: true,
            bodyPadding: '20 20',
            cls: 'auth-dialog-login',
            header: false,
            width: 415,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },

            defaults : {
                margin : '5 0'
            },

            items: [
                {
                    xtype: 'label',
                    text: 'Sign into your account'
                },
                {
                    xtype: 'textfield',
                    cls: 'auth-textbox',
                    name: 'username',
                    bind: '{username}',
                    height: 55,
                    hideLabel: true,
                    allowBlank : false,
                    emptyText: 'user id',
                    triggers: {
                        glyphed: {
                            cls: 'trigger-glyph-noop auth-email-trigger'
                        }
                    },
                    listeners:
                    {
                        specialkey: function(field, e)
                        {
                            if (e.getKey() == e.ENTER)
                            {
                                // when escape key is pressed, click the login button
                                var loginButton = field.nextSibling('button#loginButton');
                                if(loginButton)
                                    loginButton.click(e);
                            }
                        }
                    }
                },
                {
                    xtype: 'textfield',
                    cls: 'auth-textbox',
                    height: 55,
                    hideLabel: true,
                    emptyText: 'Password',
                    inputType: 'password',
                    name: 'password',
                    bind: '{password}',
                    allowBlank : false,
                    triggers: {
                        glyphed: {
                            cls: 'trigger-glyph-noop auth-password-trigger'
                        }
                    },
                    listeners:
                    {
                        specialkey: function(field, e)
                        {
                            if (e.getKey() == e.ENTER)
                            {
                                // when enter key is pressed, click the login button
                                var loginButton = field.nextSibling('button#loginButton');
                                if(loginButton)
                                    loginButton.click(e);
                            }
                        }
                    }
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'checkboxfield',
                            flex : 1,
                            cls: 'form-panel-font-color rememberMeCheckbox',
                            height: 30,
                            bind: '{persist}',
                            boxLabel: 'Remember me',
                            name: 'remember-me',
                            uncheckedValue: 'false',
                            inputValue: 'true'
                        },
                        {
                            xtype: 'box',
                            html: '<a href="#passwordreset" class="link-forgot-password"> Forgot Password ?</a>'
                        }
                    ]
                },
                {
                    xtype: 'hidden',
                    name: '_csrf',
                    value: ''//csrfToken
                },
                {
                    xtype: 'button',
                    itemId: 'loginButton',
                    reference: 'loginButton',
                    scale: 'large',
                    ui: 'soft-green',
                    iconAlign: 'right',
                    iconCls: 'x-fa fa-angle-right',
                    text: 'Login',
                    formBind: true,
                    listeners: {
                        click: 'onLoginButton',
                        afterrender: function(button, e)
                        {
                            //field.focus(false, 1000);
                            /*var loginButton = field.nextSibling('button#loginButton');
                            if(loginButton)
                                loginButton.setDisabled(false);*/
                                
                            /*var form = button.up('form');
                            if(form)
                            {
                                form.getForm().checkValidity();
                            }*/
                        }
                    }
                },
                {
                    xtype: 'box',
                    html: '<div class="outer-div"><div class="seperator">OR</div></div>',
                    margin: '10 0'
                },
                /*{
                    xtype: 'button',
                    scale: 'large',
                    ui: 'soft-blue', //'facebook'
                    iconAlign: 'right',
                    iconCls: 'x-fa fa-facebook',
                    text: 'Login with Facebook',
                    listeners: {
                        click: 'onFaceBookLogin'
                    }
                },
                {
                    xtype: 'box',
                    html: '<div class="outer-div"><div class="seperator">OR</div></div>',
                    margin: '10 0'
                },*/
                {
                    xtype: 'button',
                    scale: 'large',
                    ui: 'gray',
                    iconAlign: 'right',
                    iconCls: 'x-fa fa-user-plus',
                    text: 'Create Account',
                    listeners: {
                        click: 'onNewAccount'
                    }
                }
            ]
        }
    ],

    initComponent: function() {
        this.addCls('user-login-register-container');
        this.callParent(arguments);
    }
});
