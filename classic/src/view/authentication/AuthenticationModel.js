Ext.define('AssetRabbit.view.authentication.AuthenticationModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.authentication',

    data: {
        //userid : '',
        username : '',
        fullName : '',
        password : '',
        email    : '',
        persist: false,
        agrees : false
    }
});