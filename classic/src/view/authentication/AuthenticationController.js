Ext.define('AssetRabbit.view.authentication.AuthenticationController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.authentication',

    required: ['Ext.window.Toast'],

    //TODO: implement central Facebook OATH handling here

    onFaceBookLogin : function(button, e) {
		this.redirectTo("home", true);
    },

    onLoginButton: function(button, e, eOpts) {
        var that = this;

        var form = button.up('form');
        var params = form.getValues();
        var rest = Ext.create('AssetRabbit.common.REST');
        rest.request({
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'X-CSRF-TOKEN': csrfToken
            },
            url: '/login',
            params: params,
            callbacks: {
                success: function(result)
                {
                    var obj = Ext.decode(result.responseText);
                    if(result.status == 200)
                    {
                        sessionStorage.setItem('loggedin', 'true');

                        Ext.toast({
                            title: 'Success',
                            html: 'Successfully logged-in.',
                            align: 't'
                        });

                        that.redirectTo("home", true);
                    }    
                    else
                    {
                        sessionStorage.setItem('loggedin', 'false');

                        Ext.toast({
                            title: 'Failure',
                            html: 'Invalid username or password',
                            align: 't'
                        });
                    }
                },

                failure: function(result)
                {
                    isLoggedIn = false;
                    sessionStorage.setItem('loggedin', isLoggedIn);

                    var obj = Ext.decode(result.responseText);
                    Ext.toast({
                        title: 'Failure',
                        html: (obj.message)? obj.message : 'Unknown error occurred.',
                        align: 't'
                    });
                }
            }
        });
    },

    onLoginAsButton: function(button, e, eOpts) {
        this.redirectTo("login", true);
    },

    onNewAccount:  function(button, e, eOpts) {
        this.redirectTo("register", true);
    },

    onSignupClick:  function(button, e, eOpts) {
		this.redirectTo("home", true);
    },

    onResetClick:  function(button, e, eOpts) {
        // TODO: add a reset route
    }   

});
