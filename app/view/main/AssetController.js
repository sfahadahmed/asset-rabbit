Ext.define('AssetRabbit.view.main.AssetController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.asset',

    /*
     *
     */
    onRowSelected: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts)
    {
        var that = this;

        if(cellIndex > 0)
        {
            var win = Ext.create('AssetRabbit.view.main.DetailsForm', {
                title: 'Edit Asset',

                x: this.view.getX(),
                y: this.view.getY(),
                width: this.view.getWidth(),
                autoHeight: true,

                items: Ext.create('AssetRabbit.view.main.AssetPanel', {
                    activeRecord: record,
                    isEditForm: true,
                    grid: grid
                }),
                
                listeners: {
                    beforeclose: function(panel, e)
                    {
                        // remove open form window
                        if(openWindows['Asset'])
                           delete openWindows['Asset'];
                    }
                }
            });
            win.show();

            // close the form window when the grid view is hidden
            this.view.on('beforehide', function(){
                // if form window is visible then save the state
                if(win.isVisible())
                {
                    openWindows['Asset'] = win;
                    win.hide();
                }
            });
        }
    },

    /*
     *
     */
    onAdd: function(button, event)
    {
        var grid = this.view;

        if(grid)
        {
            var win = Ext.create('AssetRabbit.view.main.DetailsForm', {
                title: 'Add New Asset',

                x: grid.getX(),
                y: grid.getY(),
                width: grid.getWidth(),
                autoHeight: true,

                items: Ext.create('AssetRabbit.view.main.AssetPanel', {
                    isEditForm: false,
                    grid: grid
                }),

                listeners: {
                    beforeclose: function(panel, e)
                    {
                        // remove open form window
                        if(openWindows['Asset'])
                           delete openWindows['Asset'];
                    }
                }
            });
            win.show();

            // close the form window when the grid view is hidden
            grid.on('beforehide', function(){
                // if form window is visible then save the state
                if(win.isVisible())
                {
                    openWindows['Asset'] = win;
                    win.hide();
                }
            });
        }
        else
        {
            Ext.Msg.alert('Remove Asset', 'Error: Asset grid was not found');
        }
    },

    /*
     *
     */
    onRemove: function(button, event)
    {
        var grid = this.view;
        if(grid)
        {
            var store = grid.getStore();
            var sm = grid.getSelectionModel();
            if(store && sm)
            {
                var records = sm.getSelection();
                var id = records[0].get('id');

                rest = Ext.create('AssetRabbit.common.REST');
                rest.request({
                    method: 'DELETE',
                    url: '/asset/'+id,
                    callbacks: {
                        success: function(result)
                        {
                            Ext.Msg.alert('Success', 'Selected asset has been removed successfully.', function(){
                                //store.remove(sm.getSelection());
                                store.remove(records[0]);
                            });
                        },

                        failure: function(result)
                        {
                            console.log('---- FAILURE ----');
                            console.log(result);

                            if(result && result.ResponseText)
                            {
                                var response = Ext.decode(result.ResponseText);
                                Ext.Msg.alert('Error', 'Cannot removed the asset. '+(result.message)? result.message : '');
                            }
                            else
                            {
                                Ext.Msg.alert('Error', 'Cannot removed the asset.');
                            }
                        }
                    }
                });
            }
        }
        else
        {
            Ext.Msg.alert('Remove Asset', 'Error: Asset grid was not found');
        }
    },

    /*
     *
     */
    onSave: function(button, event)
    {
        // TODO: add implementation
    },

    /*
     *
     */
    onCreate: function(button, event)
    {
        // TODO: add implementation
    }
});

