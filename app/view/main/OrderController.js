Ext.define('AssetRabbit.view.main.OrderController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.order',

    requires: [],

    /*
     *
     */
    onRowSelected: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts)
    {
        if(cellIndex > 0)
        {
            var grid = Ext.getCmp('ordersGrid');
            var win = Ext.create('AssetRabbit.view.main.DetailsForm', {
                title: 'Edit Order',

                x: document.body.clientWidth - 50,
                y: grid.getY(),
                width: grid.getWidth(),
                autoHeight: true,

                items: Ext.create('AssetRabbit.view.main.OrderPanel', {
                    activeRecord: record,
                    isEditForm: true
                }),

                listeners: {
                    beforeclose: function(panel, e)
                    {
                        win.slideOutFadeOut({
                            duraion: 1000,
                            to: {
                                x: document.body.clientWidth - 50
                            },
                            afterAnimateHandler: function()
                            {
                                win.close();
                            }
                        });

                        // prevent window close
                        return false;
                    },

                    beforehide: function(panel, e)
                    {
                    },

                    beforeshow: function(panel, e)
                    {
                        win.slideInFadeIn({
                            duraion: 1000,
                            from: {
                                x: document.body.clientWidth - 50
                            },
                            to: {
                                x: grid.getX()
                            },
                            afterAnimateHandler: function()
                            {
                            }
                        });
                    }
                }
            });
            win.show();

            // close the form window when the grid view is hidden
            grid.on('beforehide', function(){
                win.close();
            });
        }
    },

    /*
     *
     */
    onAdd: function()
    {
        var grid = Ext.getCmp('ordersGrid');
        var win = Ext.create('AssetRabbit.view.main.DetailsForm', {
            title: 'Add New Order',

            x: document.body.clientWidth - 50,
            y: grid.getY(),
            width: grid.getWidth(),
            autoHeight: true,

            items: Ext.create('AssetRabbit.view.main.OrderPanel', {
                isEditForm: false
            }),

            listeners: {
                beforeclose: function(panel, e)
                {
                    win.slideOutFadeOut({
                        duraion: 1000,
                        to: {
                            x: document.body.clientWidth - 50
                        },
                        afterAnimateHandler: function()
                        {
                            win.close();
                        }
                    });

                    // prevent window close
                    return false;
                },

                beforehide: function(panel, e)
                {
                },

                beforeshow: function(panel, e)
                {
                    win.slideInFadeIn({
                        duraion: 1000,
                        from: {
                            x: document.body.clientWidth - 50
                        },
                        to: {
                            x: grid.getX()
                        },
                        afterAnimateHandler: function()
                        {
                        }
                    });
                }
            }
        });
        win.show();

        // close the form window when the grid view is hidden
        grid.on('beforehide', function(){
            win.close();
        });
    },

    /*
     *
     */
    onRemove: function()
    {
        var grid = Ext.getCmp('ordersGrid');
        if(grid)
        {
            var store = grid.getStore();
            var sm = grid.getSelectionModel();
            if(store && sm)
                store.remove(sm.getSelection());
        }
        else
        {
            Ext.Msg.alert('Remove Order', 'Error: Order grid was not found');
        }
    }
});
