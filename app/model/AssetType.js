Ext.define('AssetRabbit.model.AssetType', {
    extend: 'Ext.data.Model',

    idProperty: 'id',

    requires: [
        'AssetRabbit.model.AssetCategory'
    ],

    // relationships
    hasMany: [{
        model: 'AssetRabbit.model.AssetCategory',
        name: 'assetCategories'
    }],

    // fields
    fields: [{
        type: 'int',
        name: 'id'
    }, {
        type: 'string',
        name: 'name'
    }, {
        type: 'string',
        name: 'serialNumberFormat'
    }, {
        type: 'string',
        name: 'manufacturer'
    }, {
        type: 'string',
        name: 'manufacturerPartNumber'
    }, {
        type: 'boolean',
        name: 'capitalAsset'
    }, {
        type: 'number',
        name: 'cost'
    }, {
        type: 'string',
        name: 'description'
    }, {
        type: 'int',
        name: 'creationUser'
    }, {
        type: 'int',
        name: 'creationDate',
        convert: function(value, record)
        {
            var d = new Date(value);
            //return Ext.Date.format(d, 'm-d-Y g:i A');
            return Ext.Date.format(d, 'm/d/Y');
        }
    }, {
        type: 'string',
        name: 'assetCategory',
        mapping: 'assetCategories',
        convert: function(value, record)
        {
            // convert fetched array of category objects to array of category id's
            var newValue = '';
            if(value instanceof Array)
            {
                var ids = [];
                for(var i = 0; i < value.length; ++i)
                {
                    if(value[i])
                        ids.push(value[i].id);
                }

                newValue = ids;
            }
            else if(value && (value instanceof Object))
            {
                newValue = value.id;
            }

            return newValue;
        }
    }]
});
