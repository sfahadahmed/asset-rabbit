Ext.define('AssetRabbit.model.Order', {
    extend: 'Ext.data.Model',

    idProperty: 'id',

    fields: [{
        type: 'int',
        name: 'id'
    }, {
        type: 'string',
        name: 'description'
    }, {
        type: 'string',
        name: 'creationDate'
    }, {
        type: 'string',
        name: 'capital'
    }, {
        type: 'string',
        name: 'cost'
    }, {
        type: 'string',
        name: 'currency'
    }, {
        type: 'string',
        name: 'partNumber'
    }, {
        type: 'string',
        name: 'location'
    }, {
        type: 'string',
        name: 'poNumber'
    }, {
        type: 'string',
        name: 'nextServiceDate'
    }, {
        type: 'string',
        name: 'serialNumber'
    }, {
        type: 'string',
        name: 'status'
    }]
});
