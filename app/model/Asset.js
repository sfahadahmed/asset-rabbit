Ext.define('AssetRabbit.model.Asset', {
    extend: 'Ext.data.Model',

    idProperty: 'id',

    requires: [
        'AssetRabbit.model.AssetType',
        'AssetRabbit.model.AssetCategory',
        'AssetRabbit.model.User'
    ],

    // relationships
    hasOne: [{
        name: 'assetCategory',
        model: 'AssetRabbit.model.AssetCategory'
    }, {
        name: 'assetType',
        model: 'AssetRabbit.model.AssetType'
    }, {
        name: 'owner',
        model: 'AssetRabbit.model.User'
    }],

    // fields
    fields: [{
        type: 'int',
        name: 'id'
    }, {
        type: 'string',
        name: 'description'
    }, {
        type: 'int',
        name: 'creationDate',
        //dateFormat: 'm-d-Y g:i A'
        convert: function(value, records)
        {
            var d = new Date(value);
            //return Ext.Date.format(d, 'm-d-Y g:i A');
            return Ext.Date.format(d, 'm/d/Y');
        }
    }, {
        type: 'int',
        name: 'deliveryDate',
        //dateFormat: 'm-d-Y g:i A'
        convert: function(value, records)
        {
            var d = new Date(value);
            //return Ext.Date.format(d, 'm-d-Y g:i A');
            return Ext.Date.format(d, 'm/d/Y');
        }
    }, {
        type: 'string',
        name: 'serialNumber'
    }, {
        type: 'int',
        name: 'assetCategory',
        mapping: 'assetCategory.id'
    }, {
        type: 'int',
        name: 'assetType',
        mapping: 'assetType.id'
    }, {
        type: 'int',
        name: 'owner',
        mapping: 'owner.id'
    }]

    /*validations: [{
        type: 'length',
        field: 'description',
        min: 1
    }]*/
});
