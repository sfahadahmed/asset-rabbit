Ext.define('AssetRabbit.model.AssetCategory', {
    extend: 'Ext.data.Model',

    idProperty: 'id',

    // fields
    fields: [{
        type: 'int',
        name: 'id'
    }, {
        type: 'string',
        name: 'name'
    }, {
        type: 'string',
        name: 'description'
    }]

    /*belongsTo: [{
        name: 'assetCategory',
        model: 'AssetRabbit.model.Asset'
    }, {
        name: 'assetCategories',
        model: 'AssetRabbit.model.AssetType'
    }]*/

    /*validations: [{
        type: 'length',
        field: 'description',
        min: 1
    }]*/
});
