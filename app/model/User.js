Ext.define('AssetRabbit.model.User', {
    extend: 'Ext.data.Model',

    idProperty: 'id',

    // fields
    fields: [{
        type: 'int',
        name: 'id'
    }, {
        type: 'string',
        name: 'username'
    }, {
        type: 'string',
        name: 'name'
    }]

    /*validations: [{
        type: 'length',
        field: 'description',
        min: 1
    }]*/
});
