/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('AssetRabbit.Application', {
    extend: 'Ext.app.Application',

    name: 'AssetRabbit',

    defaultToken : 'home',

    stores: [
        // TODO: add global / shared stores here
        'NavigationTree'
    ],

    views: [
        'AssetRabbit.view.main.Viewport'
    ],

    launch: function () {
        // TODO - Launch the application

        // initialize global CSRF token variable
        csrfToken = '';

        // Activate a State Manager using State Provider.
        Ext.state.Manager.setProvider(new Ext.state.CookieProvider());

        // instantiate a local storage
        /*storage = new Ext.util.LocalStorage({
            id: 'assetrabbit',
            session: true
        });*/

        // store default value
        sessionStorage.setItem('loggedin', 'false');
        
        // stores handles to opened form windows
        openWindows = {};

        /*
        // It's important to note that this type of application could use
        // any type of storage, i.e., Cookies, LocalStorage, etc.
        var loggedIn;

        // Check to see the current value of the localStorage key
        loggedIn = localStorage.getItem("LoggedIn");

        // This ternary operator determines the value of the TutorialLoggedIn key.
        // If TutorialLoggedIn isn't true, we display the login window,
        // otherwise, we display the main view
        //Ext.create({
        //    xtype: loggedIn ? 'app-main' : 'login'
        //});
        if(loggedIn)
        {
            Ext.create({
                xtype: 'login'
            });
        }*/
    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
