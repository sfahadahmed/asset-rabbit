Ext.define('AssetRabbit.common.REST', {
    requires: [
        'Ext.Ajax'
    ],

    /*
     *
     */
    request: function(config)
    {
        var method = config.method,
        headers = config.headers;

        if(!headers)
            headers = {};

        headers['X-Application-Name'] = 'assetrabbit';

        // if REST tunneling is required, then uncomment the following lines
        //      if (method === 'DELETE' || method === 'MERGE') {
        //          headers['X-HTTP-METHOD'] = method;
        //          method = 'POST';
        //      }

        /*success  : function (response)
            {
                //var o = Ext.decode(response.responseText);
                if (config.callbacks && config.callbacks.success) {
                        config.callbacks.success(o);
                }
                else {
                        console.dir(o);
                }
            },
            failure  : function (response)
            {
                //var o = Ext.decode(response.responseText);
                //console.dir(o);
                if (config.callbacks && config.callbacks.failure) {
                        config.callbacks.failure(o);
                }
            }*/

        var cfg = {
            url: config.url,
            method: method,
            headers: headers,
            jsonData: config.params

            // configs for Cross-Origin request
            //cors: true,
            //useDefaultXhrHeader: false
        };

        //
        // setup callback functions
        //
        if(config.callbacks && config.callbacks.success)
            cfg['success'] = config.callbacks.success;

        if(config.callbacks && config.callbacks.failure)
            cfg['failure'] = config.callbacks.failure;

        Ext.Ajax.request(cfg);
    }
});