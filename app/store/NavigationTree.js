Ext.define('AssetRabbit.store.NavigationTree', {
    extend: 'Ext.data.TreeStore',

    storeId: 'NavigationTree',

    fields: [{
        name: 'text'
    }],

    root: {
        expanded: true,
        children: [
            // VISIBLE ROUTES
            {
                text:   'Home',
                viewType: 'pageblank',
                iconCls: 'x-fa fa-home fa-fw',
                //rowCls: 'nav-tree-badge nav-tree-badge-new',
                leaf:   true,
                routeId: 'home'
            }, {
                text:   'Actions',
                viewType: 'pageblank',
                iconCls: 'x-fa fa-hand-paper-o fa-fw',
                rowCls: 'nav-tree-badge nav-tree-badge-new',
                leaf:   true,
                routeId: 'actions'
            }, {
                text:   'Manage Assets',
                //viewType: 'manageassetspanel',
                viewType: 'assetsgrid',
                iconCls: 'x-fa fa-table fa-fw',
                leaf:   true,
                routeId: 'assets'
            }, {
                text:   'Create Asset',
                viewType: 'assetpanel',//'createassetspanel',
                iconCls: 'x-fa fa-table fa-fw',
                leaf:   true,
                routeId: 'createasset'
            }, {
                text:   'Asset Types',
                viewType: 'assettypesgrid',
                iconCls: 'x-fa fa-table fa-fw',
                leaf:  true,
                routeId: 'asset-types'
            }, {
                text:   'Setup Category',
                viewType: 'assetcategoriesgrid',
                iconCls: 'x-fa fa-table fa-fw',
                leaf:   true,
                routeId: 'asset-categories'
            }, /*{
                text: 'Orders',
                viewType: 'pageblank',
                leaf: true,
                iconCls: 'x-fa fa-cube',
                routeId: 'orders'
            },*/ {
                text:   'Users',
                viewType: 'usersgrid',
                iconCls: 'x-fa fa-user fa-fw',
                leaf:   true,
                routeId: 'users'
            }, {
                text: 'Administration',
                viewType: 'pageblank',
                leaf: true,
                iconCls: 'x-fa fa-gear fa-fw',
                routeId: 'administration'
            }, {
                text: 'Dashboard & Reports',
                viewType: 'pageblank',
                leaf: true,
                //iconCls: 'x-fa fa-desktop fa-fw',
                iconCls: 'x-fa fa-pie-chart fa-fw',
                routeId: 'dashboard'
            }/*, {
                text: 'Reports',
                viewType: 'pageblank',
                leaf: true,
                iconCls: 'x-fa fa-pie-chart',
                routeId: 'reports'
            },*/

            // HIDDEN ROUTES
            /*{
                viewType: 'pageblank',
                leaf:   true,
                routeId: 'home'
            }*/, {
                viewType: 'login',
                leaf: true,
                routeId: 'logout'
            }, {
                viewType: 'login',
                leaf: true,
                routeId: 'login'
            }, {
                viewType: 'register',
                leaf: true,
                routeId: 'register'
            }, {
                viewType: 'passwordreset',
                leaf: true,
                routeId: 'passwordreset'
            }, {
                viewType: 'pageblank',
                leaf: true,
                routeId: 'cart'
            }, {
                viewType: 'pageblank',
                leaf: true,
                routeId: 'settings'
            }, {
                viewType: 'pageblank',
                leaf: true,
                routeId: 'help'
            }, {
                viewType: 'pageblank',
                leaf: true,
                routeId: 'profile'
            }
        ]
    },
    fields: [
        {
            name: 'text'
        }
    ]
});
