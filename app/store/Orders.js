Ext.define('AssetRabbit.store.Orders', {
    extend: 'Ext.data.Store',
    alias: 'store.orders',
    model: 'AssetRabbit.model.Order',

    pageSize: 10,
    autoLoad: true,

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    },

    sorters: {
        direction: 'ASC',
        property: 'id'
    },

    data: {
        items: [{
            id: 201,
            description: "Dell Inspiron Laptop",
            notes: "Item will be delivered with Win10 install",
            creationDate: "4/21/15",
            deliveryDate: "5/10/15",
            assignedTo: "Jack Thompson",
            poNumber: "1234-5678",
            status: "closed"
        }, {
            id: 312,
            description: "Dell Inspiron Laptop",
            notes: "Includes bluetooh mouse",
            creationDate: "2/2/15",
            deliveryDate: "6/21/15",
            assignedTo: "Tom Lewis",
            poNumber: "8765-4321",
            status: "open"
        }]
    }
});