Ext.define('AssetRabbit.store.Assets', {
    extend: 'Ext.data.Store',
    alias: 'store.assets',
    model: 'AssetRabbit.model.Asset',

    pageSize: 10,
    autoLoad: true,

    proxy: {
        type: 'ajax',
        url : '/assets',
        //api: {
        //    read: '/assets',
        //    create: '/assets',
        //    update: '.asset',
        //    destroy: '/asset'
        //},
        //cors: true,
        //useDefaultXhrHeader : false,
        reader: {
            type: 'json'
        },
        writer: {
            type: 'json'
        }
    },

    /*proxy: {
        type: 'rest',
        url : '/assets',
        //url: 'http://localhost:8080/assets',
        reader: {
            type: 'json'
            //model: 'AssetRabbit.model.Asset'
        },
        writer: {
            type: 'json'
            //model: 'AssetRabbit.model.Asset'
        }
    },*/

    /*proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    },*/

    sorters: {
        direction: 'ASC',
        property: 'id'
    }

    /*data: {
        items: [{"id":3,"description":null,"creationDate":1441966928375,"deliveryDate":null,"serialNumber":"MBP2015_003","assetType":{"id":1,"name":"Dell Laptop X15","description":"A laptop for developers","creationDate":1441966928375,"creationUser":null,"serialNumberFormat":null,"manufacturer":"Dell","manufacturerPartNumber":"DL-X15-2300","capitalAsset":false,"cost":0.0,"assetCategories":[{"id":1,"name":"General","description":"Assets used in day-to-day business"}]},"owner":{"id":4,"username":"castillo","name":"Castillo, Mike"},"assetCategory":{"id":1,"name":"General","description":"Assets used in day-to-day business"}},{"id":4,"description":null,"creationDate":1441966928375,"deliveryDate":null,"serialNumber":"DLX15_001","assetType":{"id":2,"name":"Apple MacBook Pro Mid-2015","description":"A laptop for designers","creationDate":1441966928375,"creationUser":null,"serialNumberFormat":null,"manufacturer":"Apple","manufacturerPartNumber":"APL-MBP2015-200","capitalAsset":false,"cost":0.0,"assetCategories":[{"id":1,"name":"General","description":"Assets used in day-to-day business"}]},"owner":{"id":2,"username":"simmons","name":"Simmons, David"},"assetCategory":{"id":1,"name":"General","description":"Assets used in day-to-day business"}},{"id":5,"description":null,"creationDate":1441966928375,"deliveryDate":null,"serialNumber":"DLX15_002","assetType":{"id":2,"name":"Apple MacBook Pro Mid-2015","description":"A laptop for designers","creationDate":1441966928375,"creationUser":null,"serialNumberFormat":null,"manufacturer":"Apple","manufacturerPartNumber":"APL-MBP2015-200","capitalAsset":false,"cost":0.0,"assetCategories":[{"id":1,"name":"General","description":"Assets used in day-to-day business"}]},"owner":{"id":3,"username":"cheema","name":"Cheema, Mubashir"},"assetCategory":{"id":1,"name":"General","description":"Assets used in day-to-day business"}},{"id":8,"description":"Testing 1","creationDate":1441718665400,"deliveryDate":null,"serialNumber":null,"assetType":null,"owner":null,"assetCategory":null},{"id":9,"description":"Testing 2","creationDate":1441718665400,"deliveryDate":null,"serialNumber":"MBP2015_004","assetType":null,"owner":null,"assetCategory":null},{"id":1,"description":"This is asset #1","creationDate":1441718665400,"deliveryDate":null,"serialNumber":null,"assetType":null,"owner":null,"assetCategory":null},{"id":2,"description":"Testing REST service","creationDate":1442310848789,"deliveryDate":null,"serialNumber":null,"assetType":null,"owner":null,"assetCategory":null},{"id":14,"description":"Testing 3","creationDate":1442257200000,"deliveryDate":1443553200000,"serialNumber":"MBP2015_003","assetType":null,"owner":null,"assetCategory":null},{"id":6,"description":"Testing","creationDate":1441911600000,"deliveryDate":1796400000,"serialNumber":"DLX15_003","assetType":null,"owner":null,"assetCategory":null},{"id":17,"description":"Testing 4","creationDate":1442485833513,"deliveryDate":1442948400000,"serialNumber":"BP2015_005","assetType":null,"owner":null,"assetCategory":null}]
    }*/
});