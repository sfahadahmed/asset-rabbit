Ext.define('AssetRabbit.store.AssetTypes', {
    extend: 'Ext.data.Store',
    alias: 'store.assettypes',
    model: 'AssetRabbit.model.AssetType',

    pageSize: 10,
    autoLoad: true,

    proxy: {
        type: 'ajax',
        //api: {
        //    read: '/assettypes',
        //    create: '',
        //    update: '',
        //    destroy: ''
        //},
        url: '/assettypes',
        //cors: true,
        //useDefaultXhrHeader : false,
        reader: {
            type: 'json'
        },
        writer: {
            type: 'json'
        }
    },

    /*proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    },*/

    sorters: {
        direction: 'ASC',
        property: 'id'
    }

    /*data: {
        items: [{"id":4,"name":"Test 2","description":"blah blah blah","creationDate":1442486124465,"creationUser":null,"serialNumberFormat":"###-#####","manufacturer":"Samsung","manufacturerPartNumber":"SM-0002","capitalAsset":true,"cost":35.95,"assetCategories":[]},{"id":3,"name":"Testing 1","description":"Just for testing ...","creationDate":1442469072703,"creationUser":null,"serialNumberFormat":"###-#####","manufacturer":"Intel","manufacturerPartNumber":"Intel-00001","capitalAsset":true,"cost":39.0,"assetCategories":[]},{"id":2,"name":"Apple MacBook Pro Mid-2015","description":"A laptop for designers","creationDate":1441966928375,"creationUser":null,"serialNumberFormat":null,"manufacturer":"Apple","manufacturerPartNumber":"APL-MBP2015-200","capitalAsset":false,"cost":0.0,"assetCategories":[{"id":1,"name":"General","description":"Assets used in day-to-day business"}]},{"id":1,"name":"Dell Laptop X15","description":"A laptop for developers","creationDate":1441966928375,"creationUser":null,"serialNumberFormat":null,"manufacturer":"Dell","manufacturerPartNumber":"DL-X15-2300","capitalAsset":false,"cost":0.0,"assetCategories":[{"id":1,"name":"General","description":"Assets used in day-to-day business"}]}]
    }*/
});