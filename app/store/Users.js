Ext.define('AssetRabbit.store.Users', {
    extend: 'Ext.data.Store',
    alias: 'store.users',
    model: 'AssetRabbit.model.User',

    pageSize: 10,
    autoLoad: true,

    proxy: {
        type: 'ajax',
        url : '/users',
        //api: {
        //    read: '/users',
        //    create: '',
        //    update: '',
        //    destroy: ''
        //},
        //cors: true,
        //useDefaultXhrHeader : false,
        reader: {
            type: 'json'
        },
        writer: {
            type: 'json'
        }
    },

    /*proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    },*/

    sorters: {
        direction: 'ASC',
        property: 'id'
    }

    /*data: {
        items: [{"id":1,"username":"admin","name":"Administrator"},{"id":2,"username":"simmons","name":"Simmons, David"},{"id":3,"username":"cheema","name":"Cheema, Mubashir"},{"id":4,"username":"castillo","name":"Castillo, Mike"},{"id":5,"username":"fahad","name":"Ahmed, Fahad"},{"id":9,"username":"guest","name":"Guest"}]
    }*/
});