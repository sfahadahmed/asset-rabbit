Ext.define('AssetRabbit.store.AssetCategories', {
    extend: 'Ext.data.Store',
    alias: 'store.assetcategories',
    model: 'AssetRabbit.model.AssetCategory',

    pageSize: 10,
    autoLoad: true,

    proxy: {
        type: 'ajax',
        url : '/categories',
        //api: {
        //    read: '/categories',
        //    create: '',
        //    update: '',
        //    destroy: ''
        //},
        //cors: true,
        //useDefaultXhrHeader : false,
        reader: {
            type: 'json'
        },
        writer: {
            type: 'json'
        }
    },

    /*proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    },*/

    sorters: {
        direction: 'ASC',
        property: 'id'
    }

    /*data: {
        items: [{"id":1,"name":"General","description":"Assets used in day-to-day business"},{"id":2,"name":"Test category","description":"Testing blah blah and more blah"},{"id":3,"name":"Test 3","description":"blah blah and blah"}]
    }*/
});